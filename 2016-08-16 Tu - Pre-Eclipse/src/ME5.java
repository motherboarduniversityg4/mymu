import java.util.Scanner;

public class ME5{
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);



		System.out.println("Given unit? (C or F):");

		char unit = sc.next().charAt(0);


		if( (unit!='F'&&unit!='f') && (unit!='C'&&unit!='c') ){
		System.out.println("Not a choice");
		}

		
		else{
				System.out.println("Input temperature value:");

				double input = sc.nextDouble();

				double result=0.0;

				if (unit=='C'||unit=='c'){
				result = (input*1.8)+32.0;
				System.out.println("INPUT: "+input+ "ºC  OUTPUT: "+result+"ºF");	
				}

				if (unit=='F'||unit=='f'){
				result = (input-32.0)*(5.0/9.0);
				System.out.println("INPUT: "+input+ "ºF  OUTPUT: "+result+"ºC");
				}


		}

	}
}